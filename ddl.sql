DROP TABLE asignatura_temp_as CASCADE;
DROP SEQUENCE asignatura_temp_as_seq CASCADE;

CREATE SEQUENCE asignatura_temp_as_seq
	INCREMENT 1
	MINVALUE 1
	START 1;

CREATE TABLE asignatura_temp_as
(
   id INTEGER DEFAULT NEXTVAL('asignatura_temp_as_seq'),
	cod_asignatura VARCHAR(7)
);
