=== Run information ===

Scheme:       weka.associations.FPGrowth -P 2 -I -1 -N 10 -T 0 -C 0.9 -D 0.05 -U 932.0 -M 8.0
Relation:     cancelaciones_pedagogia
Instances:    932
Attributes:   306
[list of attributes omitted]
=== Associator model (full training set) ===

FPGrowth found 2 rules (displaying top 2)

1. [405094M   -PRACTICA PROFESIONAL I=1]: 8 ==> [405079M   -SEMINARIO DE PRACTICA PROFESIONAL I=1]: 8   <conf:(1)> lift:(116.5) lev:(0.01) conv:(7.93) 
2. [405079M   -SEMINARIO DE PRACTICA PROFESIONAL I=1]: 8 ==> [405094M   -PRACTICA PROFESIONAL I=1]: 8   <conf:(1)> lift:(116.5) lev:(0.01) conv:(7.93) 

