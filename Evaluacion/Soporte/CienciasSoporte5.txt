=== Run information ===

Scheme:       weka.associations.FPGrowth -P 2 -I -1 -N 50 -T 0 -C 0.9 -D 0.05 -U 495.0 -M 5.0
Relation:     cancelaciones_ciencias
Instances:    495
Attributes:   218
[list of attributes omitted]
=== Associator model (full training set) ===

FPGrowth found 8 rules (displaying top 8)

1. [102095M   -LABORATORIO BIOLOGIA GENERAL=1]: 5 ==> [116050M   -LABORATORIO DE QUIMICA GENERAL=1]: 5   <conf:(1)> lift:(29.12) lev:(0.01) conv:(4.83) 
2. [111062M   -CALCULO I=1, 116052M   -QUIMICA I-Q-=1]: 9 ==> [111065M   -GEOMETRIA Y ALGEBRA LINEAL=1]: 9   <conf:(1)> lift:(30.94) lev:(0.02) conv:(8.71) 
3. [111065M   -GEOMETRIA Y ALGEBRA LINEAL=1, 116053M   -LABORATORIO QUIMICA I-Q-=1]: 6 ==> [111062M   -CALCULO I=1]: 6   <conf:(1)> lift:(26.05) lev:(0.01) conv:(5.77) 
4. [111065M   -GEOMETRIA Y ALGEBRA LINEAL=1, 116053M   -LABORATORIO QUIMICA I-Q-=1]: 6 ==> [116052M   -QUIMICA I-Q-=1]: 6   <conf:(1)> lift:(33) lev:(0.01) conv:(5.82) 
5. [111065M   -GEOMETRIA Y ALGEBRA LINEAL=1, 116053M   -LABORATORIO QUIMICA I-Q-=1]: 6 ==> [111062M   -CALCULO I=1, 116052M   -QUIMICA I-Q-=1]: 6   <conf:(1)> lift:(55) lev:(0.01) conv:(5.89) 
6. [111062M   -CALCULO I=1, 111065M   -GEOMETRIA Y ALGEBRA LINEAL=1, 116053M   -LABORATORIO QUIMICA I-Q-=1]: 6 ==> [116052M   -QUIMICA I-Q-=1]: 6   <conf:(1)> lift:(33) lev:(0.01) conv:(5.82) 
7. [111062M   -CALCULO I=1, 116052M   -QUIMICA I-Q-=1, 116053M   -LABORATORIO QUIMICA I-Q-=1]: 6 ==> [111065M   -GEOMETRIA Y ALGEBRA LINEAL=1]: 6   <conf:(1)> lift:(30.94) lev:(0.01) conv:(5.81) 
8. [111065M   -GEOMETRIA Y ALGEBRA LINEAL=1, 116052M   -QUIMICA I-Q-=1, 116053M   -LABORATORIO QUIMICA I-Q-=1]: 6 ==> [111062M   -CALCULO I=1]: 6   <conf:(1)> lift:(26.05) lev:(0.01) conv:(5.77) 